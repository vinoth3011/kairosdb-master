var Client = require('node-rest-client').Client;
const {
  gzip,
  ungzip
} = require('node-gzip');
var client = new Client();
var zlib = require("zlib");
var request = require('request');
var http = require("http");
var needle = require('needle');
var data1 = [{
  "name": "test_100950_D2_P1-2-1-1-0",
  "datapoints": [
    [
      1550547105000,
      259.12
    ]
  ],
  "tags": {
    "meterno": "test"
  }
}, {
  "name": "test_100950_D2_P1-2-2-1-0",
  "datapoints": [
    [
      1550547105000,
      246.37
    ]
  ],
  "tags": {
    "meterno": "test"
  }
}];
var myJSON = JSON.stringify(data1);
console.log(myJSON)
zlib.gzip(myJSON, function(error, result) {
  console.log("data:", result);
  var options = {
    headers: {
      "Content-Encoding": "gzip",
      "Content-Type": "application/gzip; charset=UTF-8"
    }
  }
  needle.post('http://localhost:8080/api/v1/datapoints', result, options, function(err, resp) {
    console.log(err)
    console.log(resp.statusCode)
  });
});